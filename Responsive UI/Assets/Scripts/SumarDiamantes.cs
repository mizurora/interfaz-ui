using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SumarDiamantes : MonoBehaviour
{
    string totalDiamantes;
    int nDiamantes;
    public Text diamantes;

    // Start is called before the first frame update
    void Start()
    {
        nDiamantes = 10;
    }

    // Update is called once per frame
    void Update()
    {
        totalDiamantes = "" + nDiamantes;
        diamantes.text = totalDiamantes;
    }

    public void Sumar(int incremento)
    {
        nDiamantes += incremento;

    }
}
