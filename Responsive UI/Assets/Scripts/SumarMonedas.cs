using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SumarMonedas : MonoBehaviour
{
    
    string totalMonedas;
    int nMonedas;
    public Text monedas;

    // Start is called before the first frame update
    void Start()
    {
       
        nMonedas = 100;
    }

    // Update is called once per frame
    void Update()
    {
        

        totalMonedas = "" + nMonedas;
        monedas.text = totalMonedas;
    }

   

    public void Sumar (int incremento)
    {
        nMonedas += incremento;
    }
}
